# QuikBild

![](screenshot.png)

Contributors: MisterE, Zughy (hotbar background)

Code snippets from https://gitlab.com/zughy-friends-minetest/arena_lib/-/blob/master/DOCS.md and from Zughy's minigames


License: GPL 3
sounds: CC0


Description: Each player gets a turn to be the artist. In each round, the artist has a limited time to build a representation of a certain word using the tools they are given.

Other players will try to be the first to guess the word by sending it in chat (this does not spam global chat).

The artist and the guesser get a point if they guess correctly.

If no one guesses correctly, no one gets points that round. Send chat messages with keywords lowercase only, and spell them correctly!


Basic setup:

1) Type /arenas create quikbild <arena_name>
2) Type /arenas edit quikbild <arena_name>
3) Use the editor to place a minigame sign, assign it to your minigame.
4) While in the editor, move to where your arena will be.
5) Make your arena. There should be a central cage for the artist to build in, made from glass, and a viewing area around it. The entire thing should prevent escape. use walls or fullclip.
6) Using the editor tools, mark player spawner locations. These should be placed in the viewing area. Protect the arena.
7) Go to minigame settings, and open the settings editor.
8) Set the build_area_pos_1 and 2 to be locations that fully encompass the central build area. This will allow the arena to clear builds between rounds.
9) Edit the word list. Only a short example word list is given, but you can make your own, and set your arena to a theme! Make sure to use the proper lua table syntax!
10) Set the build time. This is the time allowed for the artist to build, and the time allowed for other players to guess. If this runs out, no one gets any points.
11) Set the artist spawn position. This should be a location inside the build area.
12) Exit the editor mode.
13) Type /minigamesettings quikbild 
14) Change the hub spawnpoint to be next to the signs, and adjust the queuing time as desired.

