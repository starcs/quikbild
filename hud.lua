quikbild.huds = {}
local S = minetest.get_translator("quikbild")

function quikbild.create_hud(p_name)
    local player = minetest.get_player_by_name(p_name)
    quikbild.huds[p_name] = {}
    quikbild.huds[p_name]["background"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=-1, y=1},
        scale = {x = 4, y = 4},
        offset = {x=0, y=0},

        hud_elem_type = "image",
        name = "leaderboard_bg",

        text = "quikbild_hud_bg.png",
        -- text2 = "<text>",
        -- number = 0,
        -- item = 0,
        -- direction = 0,

        -- world_pos = {x=0, y=0, z=0},
        -- size = {x=0, y=0},
        -- z_index = 0,
        -- style = 0,
    })
    quikbild.huds[p_name]["yourscore"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-40*4, y=2*4},

        hud_elem_type = "text",
        name = "leader1",

        text = "",
        number = 0XCFC6B8,
        size = {x=1.5, y=0},
        style = 1,
    })
    quikbild.huds[p_name]["leader1"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-40*4, y=6*4},

        hud_elem_type = "text",
        name = "leader1",

        text = "",
        number = 0xF4B41B,
        size = {x=1, y=0},
        style = 1,
    })
    quikbild.huds[p_name]["leader2"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-40*4, y=10*4},

        hud_elem_type = "text",
        name = "leader1",

        text = "",
        number = 0xA0938E,
        size = {x=1, y=0},
        style = 1,
    })
    quikbild.huds[p_name]["leader3"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-40*4, y=14*4},

        hud_elem_type = "text",
        name = "leader1",

        text = "",
        number = 0xA0938E,
        size = {x=1, y=0},
        style = 1,
    })
    quikbild.huds[p_name]["leader4"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-40*4, y=18*4},

        hud_elem_type = "text",
        name = "leader1",

        text = "",
        number = 0xA0938E,
        size = {x=1, y=0},
        style = 1,
    })
    quikbild.huds[p_name]["leader5"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-40*4, y=22*4},

        hud_elem_type = "text",
        name = "leader1",

        text = "",
        number = 0xA0938E,
        size = {x=1, y=0},
        style = 1,
    })


end

function quikbild.update_hud(p_name,scoreboard,yourscore)
    local player = minetest.get_player_by_name(p_name)
    player:hud_change(quikbild.huds[p_name]["yourscore"], "text", S("Your Score")..": "..yourscore)
    for i = 1,5 do
        if scoreboard[i] then
            player:hud_change(quikbild.huds[p_name]["leader".. i], "text", scoreboard[i].str)
        end
    end
end

function quikbild.remove_hud(p_name)
    local player = minetest.get_player_by_name(p_name)
    for name,id in pairs(quikbild.huds[p_name]) do
        player:hud_remove(id)
    end
    quikbild.huds[p_name] = nil
end