
-- local value settings
local player_speed = 2 -- when in the minigame
local player_jump = 2 -- when in the minigame

quikbild = {} --global table
quikbild.version = "12.26.2022.0"
quikbild.storage = minetest.get_mod_storage()


quikbild.celebration_time = 10

  arena_lib.register_minigame("quikbild", {
      prefix = "[QuikBild] ",
      name = "QuikBild",
      icon = "magiccompass_quikbild.png",
      show_minimap = false,
      show_nametags = true,
      time_mode = "incremental",
      join_while_in_progress = true,
      keep_inventory = false,
      in_game_physics = {
        speed = player_speed,
        jump = player_jump,
        sneak = false,
      },
      properties = {
        build_area_pos_1 = {x = 0, y = 0, z = 0},
        build_area_pos_2 = {x = 0, y = 0, z = 0},
        word_list_path = "/wordlists/default_wordlist.csv",
		    build_time = 120, --sec allowed to build
        artist_spawn_pos = {x = 0, y = 0, z = 0},
      },
      load_time = 4,
      celebration_time = quikbild.celebration_time,
      hotbar = {
        slots = 3,
        background_image = "quikbild_gui_hotbar3.png",
      },
      temp_properties = {
        state = 'choose_artist', --game states: 'choose_artist', 'build_think','build','game_over'
        state_time = 0,
        artist = nil,
        word = '',
        has_built = {},
		    word_list = {},
        answer_list = {},
        win_guesser = '',
        stall = false,
      },
      spectate_mode = false,
      disabled_damage_types = {"punch","fall","node_damage","set_hp","drown"},

      player_properties = {
        role = "",
        score = 0,
        lang = 1,
        kickvotes = 0,
        voters = {},
      },
  })


if not minetest.get_modpath("lib_chatcmdbuilder") then
  dofile(minetest.get_modpath("quikbild") .. "/chatcmdbuilder.lua")
end

quikbild.csv = dofile(minetest.get_modpath("quikbild") .. "/lua-csv/lua/csv.lua")

dofile(minetest.get_modpath("quikbild") .. "/hud.lua")
dofile(minetest.get_modpath("quikbild") .. "/items.lua")
dofile(minetest.get_modpath("quikbild") .. "/commands.lua")
dofile(minetest.get_modpath("quikbild") .. "/nodes.lua")
dofile(minetest.get_modpath("quikbild") .. "/minigame_manager.lua")
dofile(minetest.get_modpath("quikbild") .. "/privs.lua")
