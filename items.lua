local S = minetest.get_translator("quikbild")

minetest.register_tool("quikbild:lang", {

    description = S("Choose Language"),
    inventory_image = "quikbild_langchoose.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local p_name = user:get_player_name()
        if p_name then
            quikbild.send_lang_fs(p_name) 
        end
    end

})



minetest.register_tool("quikbild:help", {

    description = S("Help"),
    inventory_image = "arenalib_editor_info.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local p_name = user:get_player_name()
        if p_name then
            minetest.show_formspec(p_name, "qb_help", "formspec_version[5]".. 
            "size[10.5,5]".. 
            "background9[0,0;0,0;quikbild_gui_bg.png;true;5]".. 
            "textarea[0.8,0.7;8.8,3.5;Helpbox;Quikbild "..S("Help")..";"..S("Each player gets a turn to be Builder. Everyone else guesses what the Builder is building. To guess type your guess in chat. Use lowercase letters only. Answers can be 1 or 2 words. When it is your turn to be Builder you will be shown a word - build it. DO NOT build letters and do not try to tell other players what the word is. That ruins the game for everyone. No one will want to play this with you anymore. If you do not know the word use a search engine to look it up. You have time to do that!").."]")
        end
    end
})


