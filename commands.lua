local S = minetest.get_translator("quikbild")


minetest.register_chatcommand("qblang", {
  params = "",
  description = "Set Quickbild language: use /qblang <code>  Available codes: en,it,es,fr,de",
  func = function(name, param)
      quikbild.send_lang_fs(name)
  end,
})



minetest.register_chatcommand("qblang_send", {
  params = "",
  privs = { quikbild_admin = true },
  description = "Send a player the language selector formspec",
  func = function(name, param)
      if minetest.get_player_by_name(param) then
        quikbild.send_lang_fs(param)
      end
  end,
})

minetest.register_on_mods_loaded( function()
  table.insert(minetest.registered_on_chat_messages,1,function(name, message)
    local mod = arena_lib.get_mod_by_player(name)
    if mod == "quikbild" then
      local arena = arena_lib.get_arena_by_player(name)
      if arena then
        if arena.artist and arena.artist == name and arena.in_game and arena.in_celebration == false then
          if message ~= "/quit" then
            minetest.chat_send_player(name, minetest.colorize("#eea160","[!]" .. S("Chat is disabled while being the artist in quikbild. Don't cheat!")))
            return true
          end
        end
      end
    end 
  end)
end)